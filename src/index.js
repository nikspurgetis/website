const express = require('express');
const nunjucks = require('nunjucks');
const app = express();
const port = 5000;

const TAGLINES = [
    "where an amateur coder has his head in the clouds",
    "where we believe dogs should vote",
    "proudly serving HTML since 2020",
    "we'll leave the port open for you",
    "no cumulonimbi allowed >:("
];

app.use(express.static(__dirname + '/public'));

nunjucks.configure('src/views', {
    autoescape: true,
    express: app
});

app.get('/', (req, res) => {
    res.render('index.njk', {
        title: "Stratus Hill - Home",
        tagline: getRandomFromArray(TAGLINES)
    });
});

app.get('/:page', (req, res) => {
    res.render(`${req.params.page}.njk`, {
        title: `Stratus Hill - ${req.params.page}`
    });
});

app.listen(port, 'localhost', () => {
    console.log('webapp started');
});

function getRandomFromArray(array) {
    return array[Math.floor(Math.random() * array.length)];
}
